/*
 * lpc17xx_pinfunc.h
 *
 *  Created on: 27.12.2010
 *      Author: Borisov
 */

#ifndef LPC17XX_PINFUNC_H_
#define LPC17XX_PINFUNC_H_

/******************************************************************************************
 *                    PIN CON
 ******************************************************************************************/


#define LPC_TIM_EMR_EMC_NOTHING (0)
#define LPC_TIM_EMR_EMC_CLEAR   (1)
#define LPC_TIM_EMR_EMC_SET     (2)
#define LPC_TIM_EMR_EMC_TOGGLE  (3)




#define LPC_PINCON_PINSEL_PX0_Pos                (0)
#define LPC_PINCON_PINSEL_PX0_Msk                (0x03UL << LPC_PINCON_PINSEL_PX0_Pos)

#define LPC_PINCON_PINSEL_PX1_Pos                (2)
#define LPC_PINCON_PINSEL_PX1_Msk                (0x03UL << LPC_PINCON_PINSEL_PX1_Pos)

#define LPC_PINCON_PINSEL_PX2_Pos                (4)
#define LPC_PINCON_PINSEL_PX2_Msk                (0x03UL << LPC_PINCON_PINSEL_PX2_Pos)

#define LPC_PINCON_PINSEL_PX3_Pos                (6)
#define LPC_PINCON_PINSEL_PX3_Msk                (0x03UL << LPC_PINCON_PINSEL_PX3_Pos)

#define LPC_PINCON_PINSEL_PX4_Pos                (8)
#define LPC_PINCON_PINSEL_PX4_Msk                (0x03UL << LPC_PINCON_PINSEL_PX4_Pos)

#define LPC_PINCON_PINSEL_PX5_Pos                (10)
#define LPC_PINCON_PINSEL_PX5_Msk                (0x03UL << LPC_PINCON_PINSEL_PX5_Pos)

#define LPC_PINCON_PINSEL_PX6_Pos                (12)
#define LPC_PINCON_PINSEL_PX6_Msk                (0x03UL << LPC_PINCON_PINSEL_PX6_Pos)

#define LPC_PINCON_PINSEL_PX7_Pos                (14)
#define LPC_PINCON_PINSEL_PX7_Msk                (0x03UL << LPC_PINCON_PINSEL_PX7_Pos)

#define LPC_PINCON_PINSEL_PX8_Pos                (16)
#define LPC_PINCON_PINSEL_PX8_Msk                (0x03UL << LPC_PINCON_PINSEL_PX8_Pos)

#define LPC_PINCON_PINSEL_PX9_Pos                (18)
#define LPC_PINCON_PINSEL_PX9_Msk                (0x03UL << LPC_PINCON_PINSEL_PX9_Pos)

#define LPC_PINCON_PINSEL_PX10_Pos                (20)
#define LPC_PINCON_PINSEL_PX10_Msk                (0x03UL << LPC_PINCON_PINSEL_PX10_Pos)

#define LPC_PINCON_PINSEL_PX11_Pos                (22)
#define LPC_PINCON_PINSEL_PX11_Msk                (0x03UL << LPC_PINCON_PINSEL_PX11_Pos)

#define LPC_PINCON_PINSEL_PX12_Pos                (24)
#define LPC_PINCON_PINSEL_PX12_Msk                (0x03UL << LPC_PINCON_PINSEL_PX12_Pos)

#define LPC_PINCON_PINSEL_PX13_Pos                (26)
#define LPC_PINCON_PINSEL_PX13_Msk                (0x03UL << LPC_PINCON_PINSEL_PX13_Pos)

#define LPC_PINCON_PINSEL_PX14_Pos                (28)
#define LPC_PINCON_PINSEL_PX14_Msk                (0x03UL << LPC_PINCON_PINSEL_PX14_Pos)

#define LPC_PINCON_PINSEL_PX15_Pos                (30)
#define LPC_PINCON_PINSEL_PX15_Msk                (0x03UL << LPC_PINCON_PINSEL_PX15_Pos)


#define LPC_PINCON_PINSEL_PX16_Pos                (0)
#define LPC_PINCON_PINSEL_PX16_Msk                (0x03UL << LPC_PINCON_PINSEL_PX16_Pos)

#define LPC_PINCON_PINSEL_PX17_Pos                (2)
#define LPC_PINCON_PINSEL_PX17_Msk                (0x03UL << LPC_PINCON_PINSEL_PX17_Pos)

#define LPC_PINCON_PINSEL_PX18_Pos                (4)
#define LPC_PINCON_PINSEL_PX18_Msk                (0x03UL << LPC_PINCON_PINSEL_PX18_Pos)

#define LPC_PINCON_PINSEL_PX19_Pos                (6)
#define LPC_PINCON_PINSEL_PX19_Msk                (0x03UL << LPC_PINCON_PINSEL_PX19_Pos)

#define LPC_PINCON_PINSEL_PX20_Pos                (8)
#define LPC_PINCON_PINSEL_PX20_Msk                (0x03UL << LPC_PINCON_PINSEL_PX20_Pos)

#define LPC_PINCON_PINSEL_PX21_Pos                (10)
#define LPC_PINCON_PINSEL_PX21_Msk                (0x03UL << LPC_PINCON_PINSEL_PX21_Pos)

#define LPC_PINCON_PINSEL_PX22_Pos                (12)
#define LPC_PINCON_PINSEL_PX22_Msk                (0x03UL << LPC_PINCON_PINSEL_PX22_Pos)

#define LPC_PINCON_PINSEL_PX23_Pos                (14)
#define LPC_PINCON_PINSEL_PX23_Msk                (0x03UL << LPC_PINCON_PINSEL_PX23_Pos)

#define LPC_PINCON_PINSEL_PX24_Pos                (16)
#define LPC_PINCON_PINSEL_PX24_Msk                (0x03UL << LPC_PINCON_PINSEL_PX24_Pos)

#define LPC_PINCON_PINSEL_PX25_Pos                (18)
#define LPC_PINCON_PINSEL_PX25_Msk                (0x03UL << LPC_PINCON_PINSEL_PX25_Pos)

#define LPC_PINCON_PINSEL_PX26_Pos                (20)
#define LPC_PINCON_PINSEL_PX26_Msk                (0x03UL << LPC_PINCON_PINSEL_PX26_Pos)

#define LPC_PINCON_PINSEL_PX27_Pos                (22)
#define LPC_PINCON_PINSEL_PX27_Msk                (0x03UL << LPC_PINCON_PINSEL_PX27_Pos)

#define LPC_PINCON_PINSEL_PX28_Pos                (24)
#define LPC_PINCON_PINSEL_PX28_Msk                (0x03UL << LPC_PINCON_PINSEL_PX28_Pos)

#define LPC_PINCON_PINSEL_PX29_Pos                (26)
#define LPC_PINCON_PINSEL_PX29_Msk                (0x03UL << LPC_PINCON_PINSEL_PX29_Pos)

#define LPC_PINCON_PINSEL_PX30_Pos                (28)
#define LPC_PINCON_PINSEL_PX30_Msk                (0x03UL << LPC_PINCON_PINSEL_PX30_Pos)

#define LPC_PINCON_PINSEL_PX31_Pos                (30)
#define LPC_PINCON_PINSEL_PX31_Msk                (0x03UL << LPC_PINCON_PINSEL_PX31_Msk)



#define LPC_PINFUNC_P0_0_RD1                    (1 << LPC_PINCON_PINSEL_PX0_Pos)
#define LPC_PINFUNC_P0_1_TD1                    (1 << LPC_PINCON_PINSEL_PX1_Pos)
#define LPC_PINFUNC_P0_2_TXD0                   (1 << LPC_PINCON_PINSEL_PX2_Pos)
#define LPC_PINFUNC_P0_3_RXD0                   (1 << LPC_PINCON_PINSEL_PX3_Pos)
#define LPC_PINFUNC_P0_6_SSEL1                  (2 << LPC_PINCON_PINSEL_PX6_Pos)
#define LPC_PINFUNC_P0_7_SCK1                   (2 << LPC_PINCON_PINSEL_PX7_Pos)
#define LPC_PINFUNC_P0_8_MISO1                  (2 << LPC_PINCON_PINSEL_PX8_Pos)
#define LPC_PINFUNC_P0_9_MOSI1                  (2 << LPC_PINCON_PINSEL_PX9_Pos)
#define LPC_PINFUNC_P0_15_TXD1                  (1 << LPC_PINCON_PINSEL_PX15_Pos)
#define LPC_PINFUNC_P0_15_SCK0                  (2 << LPC_PINCON_PINSEL_PX15_Pos)
#define LPC_PINFUNC_P0_15_SCK                   (3 << LPC_PINCON_PINSEL_PX15_Pos)
#define LPC_PINFUNC_P0_16_RXD1                  (1 << LPC_PINCON_PINSEL_PX16_Pos)
#define LPC_PINFUNC_P0_16_SSEL0                 (2 << LPC_PINCON_PINSEL_PX16_Pos)
#define LPC_PINFUNC_P0_17_MISO0                 (2 << LPC_PINCON_PINSEL_PX17_Pos)
#define LPC_PINFUNC_P0_18_MOSI0                 (2 << LPC_PINCON_PINSEL_PX18_Pos)
#define LPC_PINFUNC_P0_22_RTS1                  (1 << LPC_PINCON_PINSEL_PX22_Pos)
#define LPC_PINFUNC_P0_26_AOUT                  (2 << LPC_PINCON_PINSEL_PX26_Pos)
#define LPC_PINFUNC_P0_29_USBD                  (1 << LPC_PINCON_PINSEL_PX29_Pos)
#define LPC_PINFUNC_P0_30_USBD                  (1 << LPC_PINCON_PINSEL_PX30_Pos)
#define LPC_PINFUNC_P1_18_USB_UP_LED            (1 << LPC_PINCON_PINSEL_PX18_Pos)
#define LPC_PINFUNC_P1_20_SCK0                  (3 << LPC_PINCON_PINSEL_PX20_Pos)
#define LPC_PINFUNC_P1_23_MISO0                 (3 << LPC_PINCON_PINSEL_PX23_Pos)
#define LPC_PINFUNC_P1_24_MOSI0                 (3 << LPC_PINCON_PINSEL_PX24_Pos)
#define LPC_PINFUNC_P1_26_PWM1_6                (2 << LPC_PINCON_PINSEL_PX26_Pos)
#define LPC_PINFUNC_P1_27_CLKOUT                (1 << LPC_PINCON_PINSEL_PX27_Pos)
#define LPC_PINFUNC_P1_30_VBUS                  (2 << LPC_PINCON_PINSEL_PX30_Pos)
#define LPC_PINFUNC_P2_9_USB_CONNECT            (1 << LPC_PINCON_PINSEL_PX9_Pos)
#define LPC_PINFUNC_P2_0_TXD1                   (2 << LPC_PINCON_PINSEL_PX0_Pos)
#define LPC_PINFUNC_P2_1_RXD1                   (2 << LPC_PINCON_PINSEL_PX1_Pos)
#define LPC_PINFUNC_P2_2_PWM                    (1 << LPC_PINCON_PINSEL_PX2_Pos)
#define LPC_PINFUNC_P2_3_PWM                    (1 << LPC_PINCON_PINSEL_PX3_Pos)
#define LPC_PINFUNC_P2_4_PWM                    (1 << LPC_PINCON_PINSEL_PX4_Pos)
#define LPC_PINFUNC_P2_5_DTR1                   (2 << LPC_PINCON_PINSEL_PX5_Pos)
#define LPC_PINFUNC_P2_10_EINT0                 (1 << LPC_PINCON_PINSEL_PX10_Pos)
#define LPC_PINFUNC_P2_11_EINT1                 (1 << LPC_PINCON_PINSEL_PX11_Pos)
#define LPC_PINFUNC_P3_25_MAT00                 (2 << LPC_PINCON_PINSEL_PX25_Pos)








//PINMODE
#define LPC_PINMODE_PULLUP                      (0)
#define LPC_PINMODE_REPEATER                    (1)
#define LPC_PINMODE_NEITHER                     (2)
#define LPC_PINMODE_PULLDOWN                    (3)


#define LPC_PINCON_PINMODE_PX_Msk(n)  LPC_PINCON_PINSEL_PX##n##_Msk
#define LPC_PINCON_PINMODE_PX_Pos(n)  LPC_PINCON_PINSEL_PX##n##_Pos


#endif /* LPC17XX_PINFUNC_H_ */
